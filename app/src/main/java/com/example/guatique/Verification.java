package com.example.guatique;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Verification extends Fragment {

    View view;
    TextView longtext;
    Button submit;

    public Verification() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_verification, container, false);

        findid();
        getData();
        Clicks();

        return view;
    }

    private void Clicks() {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog=new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_box);
                getView().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                dialog.findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Navigation.findNavController(view).navigate(R.id.action_verification_to_home2);
                        dialog.cancel();
                    }
                });

                dialog.show();


            }
        });
    }

    private void findid() {
        longtext=view.findViewById(R.id.longtext);
        submit=view.findViewById(R.id.submit);
    }
    private void getData() {
        longtext.setText(getArguments().getString("phone"));
    }

}
