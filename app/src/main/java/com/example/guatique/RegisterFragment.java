package com.example.guatique;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import android.security.ConfirmationCallback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class RegisterFragment extends Fragment {
    View view;
    Button button1;
    TextView textView1;
    EditText editphone;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_register, container, false);

        findid();
        clicks();

        return view;
    }
    private void findid() {

        textView1=view.findViewById(R.id.LOgintext);
        button1=view.findViewById(R.id.registerbutton);
        editphone=view.findViewById(R.id.editphone);

    }
    private void clicks() {
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections = RegisterFragmentDirections.actionRegisterFragmentToLoginFragment2();
                Navigation.findNavController(view).navigate(navDirections);


            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                NavDirections navDirections = RegisterFragmentDirections.actionRegisterFragmentToVerification();

               String phone= editphone.getText().toString();
               Bundle bundle =new Bundle();
               bundle.putString("phone","A verification code has been sent to the Phone Number "+phone+".Please enter the code below.");
                Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_verification,bundle);


            }
        });
    }

}
