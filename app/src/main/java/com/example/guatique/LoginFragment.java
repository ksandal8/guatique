package com.example.guatique;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class LoginFragment extends Fragment {

    View view;
    TextView register;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       view= inflater.inflate(R.layout.fragment_login, container, false);

       findid();
       clicks();

       return view;


    }


    private void findid() {
        register=view.findViewById(R.id.register);
    }
    private void clicks() {
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavDirections navDirections =LoginFragmentDirections.actionLoginFragment2ToRegisterFragment2();
                Navigation.findNavController(view).navigate(navDirections);
            }
        });
    }



}
